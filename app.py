import zipfile
import os
from Crypto.Cipher import AES
import hashlib
import binascii

def zip_folder(input_folder, password):
    print(f'Начинаю зашифровку папки {input_folder}...')
    # Создаем архив с названием input_folder.zip
    zip_file = zipfile.ZipFile(input_folder + ".zip", "w", zipfile.ZIP_DEFLATED)

    # Рекурсивно добавляем все файлы и папки из входной папки в архив
    for root, dirs, files in os.walk(input_folder):
        for file in files:
            zip_file.write(os.path.join(root, file))

    # Закрываем архив
    zip_file.close()

    # Создаем зашифрованную копию архива с названием input_folder_encrypted.zip
    with zipfile.ZipFile(input_folder + "_encrypted.zip", "w", zipfile.ZIP_DEFLATED) as enc_zip_file:
        # Открываем исходный архив
        with zipfile.ZipFile(input_folder + ".zip") as zip_file:
            # Для каждого файла в архиве
            for zip_info in zip_file.infolist():
                # Считываем его содержимое
                with zip_file.open(zip_info) as input_file:
                    # Шифруем содержимое с помощью AES
                    crypto = AES.new(password.encode('utf-8'), AES.MODE_EAX)
                    ciphertext, tag = crypto.encrypt_and_digest(input_file.read())

                    # Создаем новый объект ZipInfo и передаем его в метод writestr
                    new_zip_info = zipfile.ZipInfo(zip_info.filename, zip_info.date_time)
                    new_zip_info.compress_type = zip_info.compress_type
                    new_zip_info.comment = zip_info.comment
                    new_zip_info.extra = zip_info.extra
                    new_zip_info.create_system = zip_info.create_system
                    new_zip_info.external_attr = zip_info.external_attr
                    enc_zip_file.writestr(new_zip_info, ciphertext)
                    
                    # Добавляем тэг в комментарий к архиву
                    enc_zip_file.comment = tag



    # Удаляем исходный архив
    os.remove(input_folder + ".zip")
    
    # Открываем зашифрованный архив и устанавливаем пароль
    with zipfile.ZipFile(input_folder + "_encrypted.zip", "r") as enc_zip_file:
        enc_zip_file.setpassword(password.encode('utf-8'))

    print(f'Папка {input_folder} успешно зашифрована паролем {password}')


# def decrypt_file(encrypted_file, password, decrypted_file):
#     # Открываем зашифрованный архив и устанавливаем пароль
#     with zipfile.ZipFile(encrypted_file, "r") as enc_zip_file:
#         enc_zip_file.setpassword(password.encode('utf-8'))

#         # Получаем список файлов в архиве
#         files = enc_zip_file.namelist()

#         # Получаем зашифрованное содержимое файла
#         with enc_zip_file.open(files[0]) as input_file:
#             ciphertext = input_file.read()

#         # Декодируем содержимое с помощью пароля и AES
#         crypto = AES.new(password.encode('utf-8'), AES.MODE_EAX, enc_zip_file.comment)
#         decrypted_text = crypto.decrypt(ciphertext)

#         # Записываем расшифрованный файл на диск
#         with open(decrypted_file, 'wb') as output_file:
#             output_file.write(decrypted_text)

#     print(f'Файл {encrypted_file} успешно расшифрован паролем {password} и сохранен как {decrypted_file}')

    
if __name__ == '__main__':
    zip_folder("pass", "Q;B(U67OM];!rsIv")
    decrypt_file("pass_encrypted.zip", "Q;B(U67OM];!rsIv", "decrypted_file.txt")
   
